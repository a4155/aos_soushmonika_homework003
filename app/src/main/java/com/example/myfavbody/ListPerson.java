package com.example.myfavbody;

import java.util.ArrayList;
import java.util.List;

public class ListPerson {

    public static List<String> titles;
    public static List<Integer> images;
    public static List<Integer> body;
    public static List<Integer> head;
    public static List<Integer> leg;

    public static int clickedHead(int resId){
        int currentHeadIndex = head.indexOf(resId);
        if(currentHeadIndex == head.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return head.get(currentHeadIndex);
    };

    public static int clickedBody(int resId){
        int currentHeadIndex = head.indexOf(resId);
        if(currentHeadIndex == head.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return body.get(currentHeadIndex);
    };

    public static int clickedLeg(int resId){
        int currentHeadIndex = head.indexOf(resId);
        if(currentHeadIndex == head.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return leg.get(currentHeadIndex);
    };


    public static List<Integer> clickedPerson(){
        PersonList();

        return images;
    }

    private static void PersonList(){

        titles = new ArrayList<>();
        images = new ArrayList<>();

        titles.add("First Head");
        titles.add("Second Head");
        titles.add("Third Head");
        titles.add("Fourth Head");
        titles.add("Fifth Head");
        titles.add("Sixth Head");
        titles.add("Seventh Head");
        titles.add("Eighth Head");
        titles.add("Ninth Head");
        titles.add("Tenth Head");
        titles.add("Eleventh Head");
        titles.add("Twelfth Head");

        titles.add("First Body");
        titles.add("Second Body");
        titles.add("Third Body");
        titles.add("Fourth Body");
        titles.add("Fifth Body");
        titles.add("Sixth Body");
        titles.add("Seventh Body");
        titles.add("Eighth Body");
        titles.add("Ninth Body");
        titles.add("Tenth Body");
        titles.add("Eleventh Body");
        titles.add("Twelfth Body");

        titles.add("First Leg");
        titles.add("Second Leg");
        titles.add("Third Leg");
        titles.add("Fourth Leg");
        titles.add("Fifth Leg");
        titles.add("Sixth Leg");
        titles.add("Seventh Leg");
        titles.add("Eighth Leg");
        titles.add("Ninth Leg");
        titles.add("Tenth Leg");
        titles.add("Eleventh Leg");
        titles.add("Twelfth Leg");


        images.add(R.drawable.head1);
        images.add(R.drawable.head2);
        images.add(R.drawable.head3);
        images.add(R.drawable.head4);
        images.add(R.drawable.head5);
        images.add(R.drawable.head6);
        images.add(R.drawable.head7);
        images.add(R.drawable.head8);
        images.add(R.drawable.head9);
        images.add(R.drawable.head10);
        images.add(R.drawable.head11);
        images.add(R.drawable.head12);

        images.add(R.drawable.body1);
        images.add(R.drawable.body2);
        images.add(R.drawable.body3);
        images.add(R.drawable.body4);
        images.add(R.drawable.body5);
        images.add(R.drawable.body6);
        images.add(R.drawable.body7);
        images.add(R.drawable.body8);
        images.add(R.drawable.body9);
        images.add(R.drawable.body10);
        images.add(R.drawable.body11);
        images.add(R.drawable.body12);

        images.add(R.drawable.legs1);
        images.add(R.drawable.legs2);
        images.add(R.drawable.legs3);
        images.add(R.drawable.legs4);
        images.add(R.drawable.legs5);
        images.add(R.drawable.legs6);
        images.add(R.drawable.legs7);
        images.add(R.drawable.legs8);
        images.add(R.drawable.legs9);
        images.add(R.drawable.legs10);
        images.add(R.drawable.legs11);
        images.add(R.drawable.legs12);
    }
}
