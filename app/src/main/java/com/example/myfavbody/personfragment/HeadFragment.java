package com.example.myfavbody.personfragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import com.example.myfavbody.R;
import com.example.myfavbody.personlistener.HeadOnClickedListener;

public class HeadFragment extends Fragment {

    private int mResHead;
    private HeadOnClickedListener mHeadOnClickedListener;

    public HeadFragment(int headId){
        this.mResHead = headId;
    }

    public HeadFragment(Context context, int resHead){
        this.mResHead = resHead;
        this.mHeadOnClickedListener = (HeadOnClickedListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_head, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView mImageViewHead = view.findViewById(R.id.display_head);
        mImageViewHead.setImageResource(mResHead);

        if(mHeadOnClickedListener != null){
            mImageViewHead.setOnClickListener( v -> {
                mHeadOnClickedListener.OnHeadClicked(mResHead);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}