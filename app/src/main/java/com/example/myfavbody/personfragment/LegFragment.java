package com.example.myfavbody.personfragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import com.example.myfavbody.R;
import com.example.myfavbody.personlistener.LegOnClickedListener;

public class LegFragment extends Fragment {

    private int mResLeg;
    private LegOnClickedListener mLegOnClickedListener;

    public LegFragment(int legId){
        this.mResLeg = legId;
    }

    public LegFragment(Context context, int resLeg){
        this.mResLeg = resLeg;
        this.mLegOnClickedListener = (LegOnClickedListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_leg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView mImageViewLeg = view.findViewById(R.id.display_leg);
        mImageViewLeg.setImageResource(mResLeg);

        if(mLegOnClickedListener != null){
            mImageViewLeg.setOnClickListener( v -> {
                mLegOnClickedListener.OnLegClicked(mResLeg);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}