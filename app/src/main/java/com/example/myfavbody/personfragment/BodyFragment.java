package com.example.myfavbody.personfragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.myfavbody.R;
import com.example.myfavbody.personlistener.BodyOnClickedListener;

public class BodyFragment extends Fragment {

    private int mResBody;
    private BodyOnClickedListener mBodyOnClickedListener;

    public BodyFragment(int bodyId){
        this.mResBody = bodyId;
    }

    public BodyFragment(Context context, int resBody){
        this.mResBody = resBody;
        this.mBodyOnClickedListener = (BodyOnClickedListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_body, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView mImageViewBody = view.findViewById(R.id.display_body);
        mImageViewBody.setImageResource(mResBody);

        if(mBodyOnClickedListener != null){
            mImageViewBody.setOnClickListener( v -> {
                mBodyOnClickedListener.OnBodyClicked(mResBody);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}