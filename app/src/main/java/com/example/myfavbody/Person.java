package com.example.myfavbody;

import java.io.Serializable;

public class Person implements Serializable {
    private int head;
    private int body;
    private int leg;

    public Person(){}

    public Person(int head, int body, int leg) {
        this.head = head;
        this.body = body;
        this.leg = leg;
    }

    public int getHead() {
        return head;
    }

    public void setHead(int head) {
        this.head = head;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getLeg() {
        return leg;
    }

    public void setLeg(int leg) {
        this.leg = leg;
    }

    @Override
    public String toString() {
        return "Person{" +
                "head=" + head +
                ", body=" + body +
                ", legs=" + leg +
                '}';
    }

}
