package com.example.myfavbody;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.myfavbody.personfragment.BodyFragment;
import com.example.myfavbody.personfragment.HeadFragment;
import com.example.myfavbody.personfragment.LegFragment;
import com.example.myfavbody.personlistener.BodyOnClickedListener;
import com.example.myfavbody.personlistener.HeadOnClickedListener;
import com.example.myfavbody.personlistener.LegOnClickedListener;

public class DisplayActivity extends AppCompatActivity implements HeadOnClickedListener, LegOnClickedListener, BodyOnClickedListener {

    private Person PersonBody;

    private HeadFragment HeadFrag;
    private BodyFragment BodyFrag;
    private LegFragment LegFrag;
    FragmentTransaction TranFrag;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Intent intent = getIntent();
        PersonBody = (Person) intent.getSerializableExtra("person");
        showPerson();
    }

    @Override
    public void OnBodyClicked(int resBody) {
        int clickedOnBody = ListPerson.clickedBody(resBody);

        TranFrag = getSupportFragmentManager().beginTransaction();
        BodyFrag = new BodyFragment(this, clickedOnBody);
        TranFrag.replace(R.id.fragmentBodyView, BodyFrag, null);
        TranFrag.commit();
    }

    @Override
    public void OnHeadClicked(int resHead) {
        int clickedOnHead = ListPerson.clickedHead(resHead);

        TranFrag = getSupportFragmentManager().beginTransaction();
        HeadFrag = new HeadFragment(this, clickedOnHead);
        TranFrag.replace(R.id.fragmentHeadView, HeadFrag, null);
        TranFrag.commit();
    }

    @Override
    public void OnLegClicked(int resLeg) {
        int clickedOnLeg = ListPerson.clickedLeg(resLeg);

        TranFrag = getSupportFragmentManager().beginTransaction();
        LegFrag = new LegFragment(this, clickedOnLeg);
        TranFrag.replace(R.id.fragmentLegView, LegFrag, null);
        TranFrag.commit();
    }

    private void showPerson(){
        TranFrag = getSupportFragmentManager().beginTransaction();

        HeadFrag = new HeadFragment( this, PersonBody.getHead());
        TranFrag.replace(R.id.fragmentHeadView, HeadFrag);

        BodyFrag = new BodyFragment(this, PersonBody.getBody());
        TranFrag.replace(R.id.fragmentBodyView, BodyFrag, null);

        LegFrag = new LegFragment(this, PersonBody.getLeg());
        TranFrag.replace(R.id.fragmentLegView, LegFrag, null);

        TranFrag.commit();
    }
    
}