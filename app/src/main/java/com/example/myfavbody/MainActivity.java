package com.example.myfavbody;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;


import com.example.myfavbody.personfragment.BodyFragment;
import com.example.myfavbody.personfragment.HeadFragment;
import com.example.myfavbody.personfragment.LegFragment;

import com.example.myfavbody.personlistener.BodyOnClickedListener;
import com.example.myfavbody.personlistener.HeadOnClickedListener;
import com.example.myfavbody.personlistener.LegOnClickedListener;
import com.example.myfavbody.personlistener.OnSelectedBodyListener;


import java.util.List;

public class MainActivity extends AppCompatActivity implements HeadOnClickedListener, LegOnClickedListener, BodyOnClickedListener, OnSelectedBodyListener {

    private List<Integer> BuildBody;
    private Person PersonBody;

    private HeadFragment HeadFrag;
    private BodyFragment BodyFrag;
    private LegFragment LegFrag;

    RecyclerView dataList;
    List<String> titles;
    List<Integer> images;
    Adapter adapter;

    private FragmentTransaction TranFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showPerson();
        PersonBody = getDefaultPerson();
        BuildBody = ListPerson.clickedPerson();
        BtnSendImage();

        adapter = new Adapter(this, titles, images);
        RecyclerView mRecyclerView = findViewById(R.id.dataList);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);

        dataList.setLayoutManager(gridLayoutManager);
        dataList.setAdapter(adapter);

    }

    private void showPerson() {
        TranFrag = getSupportFragmentManager().beginTransaction();

        TranFrag.replace(R.id.fragmentBodyView, HeadFrag, null);
        TranFrag.replace(R.id.fragmentHeadView, BodyFrag, null);
        TranFrag.replace(R.id.fragmentLegView, LegFrag, null);

        TranFrag.commit();
    }

    private Person getDefaultPerson() {
        int defaultHead = R.drawable.head1;
        int defaultBody = R.drawable.body1;
        int defaultLegs = R.drawable.legs1;
        return new Person(defaultHead, defaultBody, defaultLegs);
    }

    private void changePerson(int resSelected) {
        String resourceName = this.getResources().getResourceEntryName(BuildBody.get(resSelected));
        int resourceValue = BuildBody.get(resSelected);

        if (resourceName.contains("head")) {
            PersonBody.setHead(resourceValue);
        } else if (resourceName.contains("body")) {
            PersonBody.setBody(resourceValue);
        } else {
            PersonBody.setLeg(resourceValue);
        }
    }


    private void BtnSendImage() {
        AppCompatButton ButtonNext = findViewById(R.id.buttonNext);
        ButtonNext.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
            intent.putExtra("resId", PersonBody);
            startActivity(intent);
        });
    }

    @Override
    public void OnBodyClicked(int resBody) {

        int clickedOnBody = ListPerson.clickedBody(resBody);

        TranFrag = getSupportFragmentManager().beginTransaction();
        BodyFrag = new BodyFragment(this, clickedOnBody);
        TranFrag.replace(R.id.fragmentBodyView, BodyFrag, null);
        TranFrag.commit();
    }

    @Override
    public void OnHeadClicked(int resHead) {

        int clickedOnHead = ListPerson.clickedHead(resHead);

        TranFrag = getSupportFragmentManager().beginTransaction();
        HeadFrag = new HeadFragment(this, clickedOnHead);
        TranFrag.replace(R.id.fragmentHeadView, HeadFrag, null);
        TranFrag.commit();
    }

    @Override
    public void OnLegClicked(int resLeg) {

        int clickedOnLeg = ListPerson.clickedLeg(resLeg);

        TranFrag = getSupportFragmentManager().beginTransaction();
        LegFrag = new LegFragment(this, clickedOnLeg);
        TranFrag.replace(R.id.fragmentLegView, LegFrag, null);
        TranFrag.commit();
    }

    @Override
    public void OnSelectedBody(int resSelected) {
    changePerson(resSelected);
    }

}
